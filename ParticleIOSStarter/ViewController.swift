//
//  ViewController.swift
//  ParticleIOSStarter
//
//  Created by Parrot on 2019-06-29.
//  Copyright © 2019 Parrot. All rights reserved.

import UIKit
import Speech

import Particle_SDK
class ViewController: UIViewController, SFSpeechRecognizerDelegate {
    
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognisationTask: SFSpeechRecognitionTask?

    // MARK: User variables
    let USERNAME = "jatin_verma@outlook.com"
    let PASSWORD = "kaur1234"
    
    // MARK: Device
    
    // Antonio's device
    let DEVICE_ID = "3f003b001047363333343437"
    var myPhoton : ParticleDevice?

    // MARK: Other variables
    var gameScore:Int = 0

    
    // MARK: Outlets
    
    @IBOutlet weak var TextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        //Jatin
 
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")

                // try to get the device
                self.getDeviceFromCloud()

            }
        } // end login
        
       // self.requestSpeechAuthorization()
        
    }
    @IBAction func StarAction(_ sender: Any) {
        recordAndRecognizeSpeech()
    }
    func recordAndRecognizeSpeech(){
        guard let node: AVAudioNode = audioEngine.inputNode else { return }
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat){ buffer, _ in
            self.request.append(buffer)
        }
        
        
        audioEngine.prepare()
        do{
            try audioEngine.start()
        }catch{
            return print(error)
        }
        
        
        guard let myRecognizer = SFSpeechRecognizer() else {
            return
        }
        if !myRecognizer.isAvailable{
            return
        }
        
        
        
      recognisationTask = speechRecognizer?.recognitionTask(with: request, resultHandler: {result, error in
            if let result = result{

                let bestString =  result.bestTranscription.formattedString
                self.TextLabel.text = bestString
//
//                var lastString: String = ""
//                for segment in result.bestTranscription.segments{
//                    let indexTo = bestString.index(bestString.startIndex, offsetBy: segment.substringRange.location)
//                    lastString = bestString.substring(from: indexTo)
 //               }
//self.checkForColorsSaid(resultString: lastString)
            }else if let error = error {
                print(error)
            }
            
            
        })
    }

//    func requestSpeechAuthorization(){
//        SFSpeechRecognizer.requestAuthorization{ authStatus in
//            OperationQueue.main.addOperation {
//                switch authStatus{
//                case .authorized:
//                    self.startButton.isEnabled = true
//                case.denied:
//                    self.startButton.isEnabled = false
//                    self.TextLabel.text = "User denied access to speech recognition"
//                case .restricted:
//                    self.startButton.isEnabled = false
//                    self.TextLabel.text = "Speech recognition restricted on this device"
//                case  .notDetermined:
//                    self.startButton.isEnabled = false
//                    self.TextLabel.text = "Speech recognition not yet authorized"
//                }
//            }
//        }
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
              //  self.subscribeToParticleEvents()
            }
            
        } // end getDevice()
    }
    
    
//    //MARK: Subscribe to "playerChoice" events on Particle
//    func subscribeToParticleEvents() {
//        var handler : Any?
//        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
//            withPrefix: "playerChoice",
//            deviceID:self.DEVICE_ID,
//            handler: {
//                (event :ParticleEvent?, error : Error?) in
//
//            if let _ = error {
//                print("could not subscribe to events")
//            } else {
//                print("got event with data \(event?.data)")
//                let choice = (event?.data)!
//                if (choice == "A") {
//                    self.turnParticleGreen()
//                    self.gameScore = self.gameScore + 1;
//                }
//                else if (choice == "B") {
//                    self.turnParticleRed()
//                }
//            }
//        })
//    }
    
    
    
    func turnParticleGreen() {
        
        print("Pressed the change lights button")
        
        let parameters = ["green"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    
    func turnParticleRed() {
        
        print("Pressed the change lights button")
        
        let parameters = ["red"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    
    
//    @IBAction func testScoreButtonPressed(_ sender: Any) {
//
//        print("score button pressed")
//
//        // 1. Show the score in the Phone
//        // ------------------------------
//        self.scoreLabel.text = "Score:\(self.gameScore)"
//
//        // 2. Send score to Particle
//        // ------------------------------
//        let parameters = [String(self.gameScore)]
//        var task = myPhoton!.callFunction("score", withArguments: parameters) {
//            (resultCode : NSNumber?, error : Error?) -> Void in
//            if (error == nil) {
//                print("Sent message to Particle to show score: \(self.gameScore)")
//            }
//            else {
//                print("Error when telling Particle to show score")
//            }
//        }
//
//
//
//        // 3. done!
//
//
//    }
    
}

